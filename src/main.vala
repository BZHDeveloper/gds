public static void main (string[] args) {
	Gst.init (ref args);
	Gtk.init (ref args);
	var win = new Gds.Window();
	win.realize.connect (() => {
		win.run();
	});
	win.show_all();
	Gtk.main();
}
