namespace Gds {
	public class Player : GLib.Object {
		Gst.Element playbin;
		
		construct {
			playbin = Gst.ElementFactory.make ("playbin", null);
			playbin.bus.add_watch (Priority.DEFAULT, (bus, message) => {
				if (message.type == Gst.MessageType.EOS) {
					track = finished();
				}
				return Source.CONTINUE;
			});
			
			notify["track"].connect (() => {
				playbin.set_state (Gst.State.NULL);
				playbin["uri"] = track.uri;
				playbin.set_state (Gst.State.PLAYING);
			});
		}
		
		public signal Track finished();
		
		public int64 duration {
			get {
				int64 dur = 0;
				playbin.query_duration (Gst.Format.TIME, out dur);
				return dur;
			}
		}
		
		public int64 position {
			get {
				int64 pos = 0;
				playbin.query_position (Gst.Format.TIME, out pos);
				return pos;
			}
			set {
				playbin.seek_simple (Gst.Format.TIME, Gst.SeekFlags.FLUSH | Gst.SeekFlags.KEY_UNIT, value);
			}
		}
		
		public Track track { get; set; }
	}
}
