namespace Gds {
	public class Track : GLib.Object {
		public string uri { get; construct; }
		public string topic { get; construct; }
		public string title { get; construct; }
		
		public Track (string title, string topic, string uri) {
			GLib.Object (title : title, topic : topic, uri : uri);
		}
	}
	
	public static Gee.List<Track> open () throws GLib.Error {
		uint8[] data;
		File.new_for_uri ("http://gerarddesuresnes.fr/wp-content/fmp-jw-files/playlists/fmp_jw_widget_playlist_al.xml").load_contents (null, out data, null);
		string xml = ((string)data).replace ("_&_", "_&amp;_");
		var document = new GXml.XDocument.from_string (xml);
		var list = new Gee.ArrayList<Track>();
		list.add_all_iterator (document.get_elements_by_tag_name ("track").map<Track>(item => {
			var annotation = item.query_selector ("annotation");
			var topic = annotation.node_value.substring (0, annotation.node_value.index_of ("-")).strip();
			var title = annotation.node_value.substring (1 + annotation.node_value.index_of ("-")).strip();
			var location = item.query_selector ("location");
			return new Track (title, topic, location.node_value);
		}));
		return list;
	}
}
