namespace Gds {	
	public class Box : Gtk.Widget {
		class construct {
			set_layout_manager_type (typeof (Gtk.BinLayout));
		}
		
		construct {
			notify.connect (spec => {
				if (spec.name == "child") {
					if (child is Gtk.Widget)
						child.set_parent (this);
				}
			});
		}
		
		public override void compute_expand_internal (out bool h, out bool v) {
			if (!(child is Gtk.Widget))
				return;
			h = child.compute_expand (Gtk.Orientation.HORIZONTAL);
			v = child.compute_expand (Gtk.Orientation.VERTICAL);
		}
		
		public override Gtk.SizeRequestMode get_request_mode() {
			if (child is Gtk.Widget)
				return child.get_request_mode();
			return Gtk.SizeRequestMode.CONSTANT_SIZE;
		}
		
		public Gtk.Widget child { get; set; }
	}
	
	public class TrackRow : Gtk.ListBoxRow {
		public TrackRow (Track track, int index) {
			GLib.Object (track : track, index : index);
		}
		
		construct {
			var box = new Gtk.Box (Gtk.Orientation.VERTICAL, 5);
			var lbl_topic = new Gtk.Label (null);
			lbl_topic.use_markup = true;
			lbl_topic.set_markup ("<b><i>%s</i></b>".printf (track.topic));
			box.insert_child_after (lbl_topic, null);
			box.insert_child_after (new Gtk.Label (track.title), lbl_topic);
			set_child (box);
		}
		
		public int index { get; construct; }
		
		public Track track { get; construct; }
	}
	
	public delegate void ListBoxRowFunc (TrackRow row);
	
	public class ListBox : Box {
		Gtk.ListBox list_box;
		
		construct {
			list_box = new Gtk.ListBox();
			child = list_box;
			list_box.selection_mode = Gtk.SelectionMode.SINGLE;
			tracks = Gds.open();
			int idx = 0;
			tracks.foreach (track => {
				list_box.append (new TrackRow (track, idx));
				idx++;
				return true;
			});
			
			list_box.row_activated.connect (row => {
				var index = (row as TrackRow).index;
				track_activated (tracks[index]);
			});
		}
		
		public Gee.List<Track> tracks { get; private set; }
		
		public int size {
			get {
				return tracks.size;
			}
		}
		
		void foreach (ListBoxRowFunc func) {
			for (var i = 0; i < tracks.size; i++) {
				var row = list_box.get_row_at_index (i);
				if (row is TrackRow) {
					var trow = row as TrackRow;
					func (trow);
				}
			}
		}
		
		public signal void track_activated (Track track);
		
		public Track random() {
			var num = Random.int_range (0, size);
			int i = 0;
			this.foreach (row => {
				if (i == num)
					list_box.select_row (row);
				i++;
			});
			return tracks[num];
		}
		
		public TrackRow get_row (int index) {
			return list_box.get_row_at_index (index) as TrackRow;
		}
		
		public void select_row (TrackRow row) {
			list_box.select_row (row);
		}
		
		public void invalidate_filter() {
			list_box.invalidate_filter();
		}
		
		public void set_filter_func (Gtk.ListBoxFilterFunc func) {
			list_box.set_filter_func (func);
		}
	}
	
	public class Window : Gtk.Window {
		ListBox listbox;
		Gtk.Entry entry;
		PlayerWidget widget;
		MainLoop loop;
		
		public void run() {
			(this as Gtk.Widget).realize.connect (() => {
				var size = listbox.size;
				var index = Random.int_range (0, size);
				var row = listbox.get_row (index);
				var notif = new Notify.Notification (row.track.title, null, null);
				notif.set_icon_from_pixbuf (new Gdk.Pixbuf.from_resource ("/resources/logos/icon.png"));
				notif.show();
				this.listbox.select_row (row);
				row.activate();
			});
			show();
			loop.run();
		}
		
		construct {
			Notify.init ("gds");
			Gtk.Window.set_default_icon_name ("gds");
			
			loop = new MainLoop();
			(this as Gtk.Widget).unrealize.connect (loop.quit);
			
			widget = new PlayerWidget();
			entry = new Gtk.Entry();
			entry.set_icon_from_icon_name (Gtk.EntryIconPosition.PRIMARY, "edit-clear");
			var bar = new Gtk.HeaderBar();
			bar.title_widget = widget;
			bar.pack_start (entry);
			bar.show_title_buttons = true;
			set_titlebar (bar);
			
			var sw = new Gtk.ScrolledWindow();
			sw.hscrollbar_policy = Gtk.PolicyType.NEVER;
			listbox = new ListBox();
			sw.set_child (listbox);
			set_child (sw);
			
			listbox.track_activated.connect (track => {
				widget.player.track = track;
			});
			
			entry.changed.connect (() => {
				listbox.invalidate_filter();
			});
			
			entry.icon_release.connect (pos => {
				if (pos == Gtk.EntryIconPosition.PRIMARY)
					entry.text = "";
			});
			
			listbox.set_filter_func (row => {
				var title = (row as TrackRow).track.title.down();
				return entry.text == null || entry.text.length == 0 || title.contains (entry.text.down());
			});
			
			widget.player.finished.connect (() => {
				var track = listbox.random();
				var index = listbox.tracks.index_of (track);
				var size = listbox.tracks.size;
				double pos = sw.get_vadjustment().get_upper() * (double)index / (double)size;
				sw.get_vadjustment().set_value (pos);
				var notif = new Notify.Notification (track.title, null, null);
				notif.set_icon_from_pixbuf (new Gdk.Pixbuf.from_resource ("/resources/logos/icon.png"));
				notif.show();
				return track;
			});
			
			set_size_request (800, 600);
		}
	}

	public class Label : Box {
		int pos;
		Gtk.Label label;
		
		construct {
			label = new Gtk.Label (null);
			Timeout.add_full (Priority.DEFAULT, 300, () => {
				if (text == null)
					return Source.CONTINUE;
				var sb1 = new StringBuilder();
				var sb2 = new StringBuilder();
				unichar u = 0;
				int i = 0;
				while (text.get_next_char (ref i, out u)) {
					if (i < pos)
						sb1.append_unichar (u);
					else
						sb2.append_unichar (u);
				}
				label.set_label (sb2.str + " " + sb1.str);
				pos++;
				if (pos > text.length)
					pos = 0;
				return Source.CONTINUE;
			});
		}
		
		public string text {
			owned get {
				return label.label;
			}
			set {
				label.label = value;
			}
		}
	}
	
	public class PlayerWidget : Gtk.Box {
		Gtk.Scale scale;
		Gtk.Label label_position;
		Gtk.Label label_duration;
		Gtk.Label label_title;
		bool update;
		
		static string time_to_string (int64 val) {
			int64 s = val / 1000000000;
			int64 min = s / 60;
			s = s - 60 * min;
			int64 h = min / 60;
			min = min - 60 * h;
			if (h == 0)
				return "%02lld:%02lld".printf (min, s);
			return "%lld:%02lld:%02lld".printf (h, min, s);
		}
		
		construct {
			orientation = Gtk.Orientation.VERTICAL;
			player = new Player();
			var hbox = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 5);
			label_title = new Gtk.Label("");
			label_position = new Gtk.Label ("--:--");
			label_duration = new Gtk.Label ("--:--");
			hbox.insert_child_after (label_position, null);
			hbox.insert_child_after (label_title, label_position);
			hbox.insert_child_after (label_duration, label_title);
			insert_child_after (hbox, null);
			scale = new Gtk.Scale.with_range (Gtk.Orientation.HORIZONTAL, 0, 1000, 1);
			scale.draw_value = false;
			scale.set_size_request (400, 24);
			insert_child_after (scale, hbox);
			
			scale.change_value.connect ((st, val) => {
				update = true;
				player.position = (int64)(player.duration * val / 1000);
				update = false;
				return false;
			});
			
			player.notify["track"].connect (() => {
				label_title.label = player.track.title;
			});
			
			Timeout.add_full (Priority.DEFAULT, 500, () => {
				label_position.label = time_to_string (player.position);
				label_duration.label = time_to_string (player.duration);
				if (update)
					return Source.CONTINUE;
				if (player.duration > 0)
					scale.set_value ((double)player.position * 1000 / player.duration);
				return Source.CONTINUE;
			});
		}
		
		public Player player { get; private set; }
	}

}
