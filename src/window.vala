namespace Gds {
	public class TrackRow : Gtk.ListBoxRow {
		public TrackRow (Track track, int index) {
			GLib.Object (track : track, index : index);
		}
		
		construct {
			var box = new Gtk.Box (Gtk.Orientation.VERTICAL, 5);
			var lbl_topic = new Gtk.Label (null);
			lbl_topic.use_markup = true;
			lbl_topic.set_markup ("<b><i>%s</i></b>".printf (track.topic));
			box.pack_start (lbl_topic, false, false);
			box.pack_end (new Gtk.Label (track.title), false, false);
			add (box);
		}
		
		public int index { get; construct; }
		
		public Track track { get; construct; }
	}
	
	public class ListBox : Gtk.ListBox {
		construct {
			selection_mode = Gtk.SelectionMode.SINGLE;
			tracks = Gds.open();
			int idx = 0;
			tracks.foreach (track => {
				add (new TrackRow (track, idx));
				idx++;
				return true;
			});
			
			row_activated.connect (row => {
				var index = (row as TrackRow).index;
				track_activated (tracks[index]);
			});
		}
		
		public Gee.List<Track> tracks { get; private set; }
		
		public int size {
			get {
				return tracks.size;
			}
		}
		
		public signal void track_activated (Track track);
		
		public Track random() {
			var num = Random.int_range (0, size);
			int i = 0;
			this.foreach (row => {
				if (i == num)
					select_row (row as Gtk.ListBoxRow);
				i++;
			});
			return tracks[num];
		}
	}
	
	public class Window : Gtk.Window {
		ListBox listbox;
		Gtk.Entry entry;
		PlayerWidget widget;
		
		public void run() {
			realize.connect (() => {
				var size = (int)this.listbox.get_children().length();
				var index = Random.int_range (0, size);
				var row = this.listbox.get_row_at_index (index) as TrackRow;
				var notif = new Notify.Notification (row.track.title, null, null);
				notif.set_icon_from_pixbuf (new Gdk.Pixbuf.from_resource ("/resources/logos/icon.png"));
				notif.show();
				this.listbox.select_row (row);
				row.activate();
			});
			show_all();
			Gtk.main();
		}
		
		construct {
			icon = new Gdk.Pixbuf.from_resource ("/resources/logos/icon.png");
			Notify.init ("gds");
			
			destroy.connect (Gtk.main_quit);
			
			widget = new PlayerWidget();
			entry = new Gtk.Entry();
			entry.set_icon_from_icon_name (Gtk.EntryIconPosition.PRIMARY, "edit-clear");
			var bar = new Gtk.HeaderBar();
			bar.pack_start (entry);
			bar.set_custom_title (widget);
			bar.show_close_button = true;
			set_titlebar (bar);
			
			var sw = new Gtk.ScrolledWindow (null, null);
			sw.hscrollbar_policy = Gtk.PolicyType.NEVER;
			var sc = sw.get_vscrollbar() as Gtk.Scrollbar;
			listbox = new ListBox();
			sw.add (listbox);
			add (sw);
			
			listbox.track_activated.connect (track => {
				set_title (track.title);
				widget.player.track = track;
			});
			
			entry.changed.connect (() => {
				listbox.invalidate_filter();
			});
			
			entry.icon_release.connect ((pos, evt) => {
				if (pos == Gtk.EntryIconPosition.PRIMARY)
					entry.text = "";
			});
			
			listbox.set_filter_func (row => {
				var title = (row as TrackRow).track.title.down();
				return entry.text == null || entry.text.length == 0 || title.contains (entry.text.down());
			});
			
			widget.player.finished.connect (() => {
				var track = listbox.random();
				set_title (track.title);
				var index = listbox.tracks.index_of (track);
				var size = listbox.tracks.size;
				double pos = sc.adjustment.get_upper() * (double)index / (double)size;
				sc.set_value (pos);
				var notif = new Notify.Notification (track.title, null, null);
				notif.set_icon_from_pixbuf (new Gdk.Pixbuf.from_resource ("/resources/logos/icon.png"));
				notif.show();
				return track;
			});
			
			set_size_request (800, 600);
		}
	}
	
	public class Label : Gtk.Label {
		int pos;
		
		construct {
			Timeout.add_full (Priority.DEFAULT, 300, () => {
				if (text == null)
					return Source.CONTINUE;
				var sb1 = new StringBuilder();
				var sb2 = new StringBuilder();
				unichar u = 0;
				int i = 0;
				while (text.get_next_char (ref i, out u)) {
					if (i < pos)
						sb1.append_unichar (u);
					else
						sb2.append_unichar (u);
				}
				label = sb2.str + " " + sb1.str;
				pos++;
				if (pos > text.length)
					pos = 0;
				return Source.CONTINUE;
			});
		}
		
		public string text { get; set; }
	}
	
	public class PlayerWidget : Gtk.Box {
		Gtk.Scale scale;
		Gtk.Label label_position;
		Gtk.Label label_duration;
		Gtk.Label label_title;
		bool update;
		
		static string time_to_string (int64 val) {
			int64 s = val / 1000000000;
			int64 min = s / 60;
			s = s - 60 * min;
			int64 h = min / 60;
			min = min - 60 * h;
			if (h == 0)
				return "%02lld:%02lld".printf (min, s);
			return "%lld:%02lld:%02lld".printf (h, min, s);
		}
		
		construct {
			orientation = Gtk.Orientation.VERTICAL;
			player = new Player();
			var hbox = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 5);
			label_title = new Gtk.Label("");
			label_position = new Gtk.Label ("--:--");
			label_duration = new Gtk.Label ("--:--");
			hbox.pack_start (label_position, false, false);
			hbox.pack_end (label_duration, false, false);
			hbox.set_center_widget (label_title);
			pack_start (hbox, false, false);
			scale = new Gtk.Scale.with_range (Gtk.Orientation.HORIZONTAL, 0, 1000, 1);
			scale.draw_value = false;
			scale.set_size_request (400, 24);
			pack_start (scale);
			
			scale.change_value.connect ((st, val) => {
				update = true;
				player.position = (int64)(player.duration * val / 1000);
				update = false;
				return false;
			});
			
			player.notify["track"].connect (() => {
				label_title.label = player.track.title;
			});
			
			Timeout.add_full (Priority.DEFAULT, 500, () => {
				label_position.label = time_to_string (player.position);
				label_duration.label = time_to_string (player.duration);
				if (update)
					return Source.CONTINUE;
				if (player.duration > 0)
					scale.set_value ((double)player.position * 1000 / player.duration);
				return Source.CONTINUE;
			});
		}
		
		public Player player { get; private set; }
	}
}
